import std.stdio;

class ArgumentError : Exception {
    this(string msg, string file = __FILE__, size_t line = __LINE__) {
        super(msg, file, line);
    }
}

void main () {
    try {
        throw new Exception("error message");
    } catch (Exception error) {
        writefln("Error catched: %s", error.msg);
    } finally {
        writefln("in finaly block");
    }

    try {
        throw new ArgumentError("error message");
    } catch (ArgumentError error) {
        writefln("Error catched: %s", error.msg);
    }

}